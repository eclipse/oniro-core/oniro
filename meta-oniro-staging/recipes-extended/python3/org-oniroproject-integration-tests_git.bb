# SPDX-FileCopyrightText: Le Van Quach <levan.quach@kalpa.it>
#
# SPDX-License-Identifier: MIT

SUMMARY = "Oniro project integration tests provider"
HOMEPAGE = "https://gitlab.eclipse.org/eclipse/oniro-core/oniro-testing"

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://LICENSES/APACHE-2.0.txt;md5=349c658c6b88384e679a1adcc5c63c33"
SRC_URI = "git://gitlab.eclipse.org/eclipse/oniro-core/oniro-testing;protocol=https;branch=main"
SRC_URI[sha256sum] = "8e05621638d052171bd67274da2b13176c5e2d6429bd995b2dfe73d079d9f1a9"

SRCREV = "d924b73bdb46356b63a89195f8cc203754b5441a"
S = "${WORKDIR}/git"

inherit python3native

DEPENDS += "python3-checkbox-ng-native"

CFLAGS:libc-musl += "-D_GNU_SOURCE"

do_compile () {
    # Replace
    # cd ${B}/org.oniroproject.integration-tests && ./manage.py build
    # manage.py executes make but the environment used by python is not related to yocto environment.
    # We must avoid host contamination with manage.py by using oe_runmake for compilation in the same way
    # manage.py uses make.
    mkdir -p ${B}/org.oniroproject.integration-tests/build/bin
    cd ${B}/org.oniroproject.integration-tests/build/bin
    export PLAINBOX_SRC_DIR=../../src
    oe_runmake -f ../../src/Makefile VPATH=../../src
    cd -
}

do_install () {
    ${S}/org.oniroproject.integration-tests/manage.py install --root=${D}
}

FILES:${PN} += " /usr/local/*"

RDEPENDS:${PN} = " \
    bluez5 \
    iproute2 \
    libgudev \
    python3-checkbox-ng-service \
    python3-checkbox-support \
    python3-dbus \
    python3-natsort \
    python3-pygobject \
    sysstat \
    ${@bb.utils.contains("DISTRO_FEATURES", "polkit", "udisks2", "", d)} \
    v4l-utils \
"

RDEPENDS:${PN}:append:x86 = " fwts"
RDEPENDS:${PN}:append:x86-64 = " fwts"
RDEPENDS:${PN}:append:aarch64 = " fwts"
RDEPENDS:${PN}:append:powerpc64 = " fwts"

RDEPENDS:${PN}:rpi += " \
    rpi-gpio \
"

.. SPDX-FileCopyrightText: Linaro Ltd
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: definitions.rst

.. _UpdateTool:

(EXPERIMENTAL) Update Tool
##########################

While developing a product based on |main_project_name|, developers add layers,
configuration options and more. To make sure they follow the latest bugfixes and
security fixes, developers should have a way to easily update their development
to the latest |main_project_name| version. The update tool (currently experiemental)
allows exactly that.

Assumptions
***********

The tool assumes that users do not modify upstream layers delivered
with |main_project_name|. Instead, they perform all changes in their own layers using
new recipes and .bbappend files.

Users are expected to modify their bblayers.conf and local.conf, those files will be
updated, carrying forward changes done by the user.

.. Note::

   When using the update tool, all modifications done directly on layers delivered with
   |main_project_name| will be overwritten. They are still available in the backup
   created by the tool before the update. Please move them to additional layers.
   Please refer to [the Yocto Project best practices](https://docs.yoctoproject.org/dev-manual/common-tasks.html#following-best-practices-when-creating-layers)
   for more information how to create your own layers.

Usage
*****

The script includes help allowing to use different options:

.. code-block:: console

    $ python3 ./oniro/scripts/upgrade_oniro/upgrade_oniro.py --help
    usage: upgrade_oniro.py [-h] {build,upgrade,backup,restore} ...

    [WIP] A tool to upgrade to a newer version of Oniro

    positional arguments:
      {build,upgrade,backup,restore}
        build               Build target with current Oniro setup.
        upgrade             Upgrade Oniro.
        backup              Backup Oniro manually.
        restore             Restore a local backup.

    options:
      -h, --help            show this help message and exit


Most users will use the `upgrade` command. It accepts a number of parameters. Out of
the list, the most important are `-tv` (target version) allowing to specify the target
branch or release (for example `kirkstone`), `-f` specifying the flavour (linux by default).
The user should also specify the build directory which will be used to collect the
configuration files and build the new version (if requested), and the |main_project_name|
repository path, without the `oniro` directory name.

An example upgrade command log is shown below. It is an upgrade from `2.0.0-alpha2`
tag to the latest version of the `kirkstone` branch. The log includes a complete list of
files changed between the two versions. The script asks some interactive questions,
for example if the user wants to perform a build of the updated image.

.. code-block:: console

    $ python3 ./oniro/scripts/upgrade_oniro/upgrade_oniro.py upgrade -tv kirkstone -f linux /home/oniro/oniro-to-update/build-oniro-linux/ /home/oniro/oniro-to-update/
    In case of errors in the procedure, or if you want to revert the update, it's possibile to restore a previous backup
     .gitignore                                         |     1 +
     .gitlab-ci.yml                                     |   638 +-
     .gitlab/issue_templates/Bug.md                     |     2 +-
     .oniro-ci/build-generic.yaml                       |   166 +-
     .oniro-ci/containers/bitbake-builder/.gitconfig    |     4 +-
     [...]
     scripts/cve-diff.py (new)                          |   322 +
     scripts/cve-report.py (new)                        |   177 +
     scripts/recipes-with-no-cves.py (new)              |   125 +
     scripts/upgrade_oniro/upgrade_oniro.py (new +x)    |   901 +
     scripts/upgrade_oniro/upgrade_oniro_utils.py (new) |   442 +
     337 files changed, 39981 insertions(+), 1422 deletions(-)

    The above changes will be applied, do you want to continue? [y/n]
    y
    Previous HEAD position was 47c36be Bump release to version 2.0.0-alpha2
    Switched to branch 'kirkstone'

    Your branch is up to date with 'origin/kirkstone'.

    Fetching: 100% (22/22), done in 35.843s
    Garbage collecting: 100% (22/22), done in 0.311s

    Thank you for building a development version of Oniro Project.
    This manifest file automatically follows changes in certain
    layers and is suitable for development of Oniro Project itself.

    Check out Oniro Project documentation at https://docs.oniroproject.org

    Chat with community in Oniro online's IRC channel as explained at:
    https://docs.oniroproject.org/en/latest/community-chat-platform.html
    or directly join to the #oniro channel at https://libera.chat

    Do you wish to update the .bbappend files in your own custom layers? [y/n]
    y
    Updating bblayers...Done
    Updating oniro.inc if necessary...Done
    Removing build-oniro-linux/tmp
    Would you like to build the target image? [y/n]
    n
    build-oniro-linux/conf/bblayers.conf has been updated
    /home/oniro/oniro-to-update/oniro/meta-oniro-core/conf/distro/include/oniro.inc has been updated
